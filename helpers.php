<?php
/**
 * Loguje podatke
 * @param mixed $data 
 */
function data_log($data)
{
    $filename = "/var/www/html/test/logs/text.log";
    if (!file_exists($filename)) 
    {
        mkdir("/var/www/html/test/logs/");
    }
    $msg = "";
    $file = fopen($filename, "a+");
    if(gettype($data) == 'NULL' || gettype($data) == null) {
        $msg = "NULL \n";
    }   
    else if (is_string($data)) {
         $msg = "\n".$data."\n";
    } 
    else if (empty($data) || $data == "") {
            $msg = "Empty \n";
    } 
    else if (is_bool($data)) {
        $msg = $data ? "TRUE\n" : "FALSE\n";
    }
    else {
        $msg = print_r($data, true);
    }
    $msg .= "_______________________________________________________________________________".PHP_EOL;
    fwrite($file, $msg);
    fclose($file);

//    chmod("/var/www/html/test/logs/text.log", 0777);
}
